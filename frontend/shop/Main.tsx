import * as React from 'react';
import { Link } from "react-router-dom";

class Main extends React.Component<any, any> {
  render() {
    return (
      <div>
        Home
        <br/>
        <Link to={'/admin'}>AdminPanel</Link>
      </div>
    )
  }
}

export default Main;