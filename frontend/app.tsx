import * as React from "react";
import ReactDOM from 'react-dom';
import {MainRouter} from "./MainRouter";
import "./style/reset.scss";
import "./style/fonts.scss";
import "./style/style.scss";

ReactDOM.render(<MainRouter/>, document.getElementById('root'));