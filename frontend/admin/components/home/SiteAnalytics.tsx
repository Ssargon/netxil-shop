import * as React from "react";
import {Grid} from "@material-ui/core";
import {useStyles} from "./style";
import {Collection, Cube, Users} from "heroicons-react";

interface Props {

}

export const SiteAnalytics: React.FC<Props> = ({}) => {
  const classes = useStyles();
  return (
    <Grid container spacing={5}>
      <Grid item md={4}>
        <div className={classes.blockAnalytick} style={{background: '#36344F'}}>
          <div className={classes.blockAnalytickHeader}>
            Categories
          </div>
          <div className={classes.blockAnalytickContent}>
            <Collection size={80} color='#fff'/><span>7</span>
          </div>
          <div className={classes.blockAnalytickFooter}>
            <span>Today</span><span>+1</span>
          </div>
        </div>
      </Grid>
      <Grid item md={4}>
        <div className={classes.blockAnalytick} style={{background: '#C24632'}}>
          <div className={classes.blockAnalytickHeader}>
            Products
          </div>
          <div className={classes.blockAnalytickContent}>
            <Cube size={80} color='#fff'/> <span>74</span>
          </div>
          <div className={classes.blockAnalytickFooter}>
            <span>Today</span><span>+14</span>
          </div>
        </div>
      </Grid>
      <Grid item md={4}>
        <div className={classes.blockAnalytick} style={{background: '#CFA81D'}}>
          <div className={classes.blockAnalytickHeader}>
            Users
          </div>
          <div className={classes.blockAnalytickContent}>
            <Users size={80} color='#fff'/><span>23</span>
          </div>
          <div className={classes.blockAnalytickFooter}>
            <span>Today</span><span>+2</span>
          </div>
        </div>
      </Grid>
    </Grid>
  )
}