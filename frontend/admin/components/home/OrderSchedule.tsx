import * as React from "react";
import {Area, AreaChart, CartesianGrid, Tooltip, XAxis, YAxis} from "recharts";
import {data} from "./data";
import {useStyles} from "./style";

interface Props {

}

export const OrderSchedule: React.FC<Props> = ({}) => {
  const classes = useStyles();
  return (
    <div className={classes.scheduleWrapper}>
      <h2>Orders Schedule</h2>
      <AreaChart width={1400} height={420} data={data}>
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
            <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
          </linearGradient>
        </defs>
        <XAxis dataKey="name"/>
        <YAxis/>
        <CartesianGrid strokeDasharray="3 3"/>
        <Tooltip/>
        <Area type="monotone" dataKey="orders" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)"/>
      </AreaChart>
    </div>
  )
}