import * as React from "react";
import {SiteAnalytics} from "./SiteAnalytics";
import {OrderSchedule} from "./OrderSchedule";

interface Props {

}

export const Home: React.FC<Props> = ({}) => {
  return (
    <React.Fragment>
      <SiteAnalytics/>
      <OrderSchedule/>
    </React.Fragment>
  )
}