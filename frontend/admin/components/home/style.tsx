import {makeStyles} from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  blockAnalytick: {
    width: '100%',
    boxShadow: '2px 2px 8px -1px #5111F3',
    fontFamily: 'Roboto Slab',
    color: '#fff',
    transition: '150ms',
    '&:hover': {
      transition: '150ms',
      marginTop: -3,
      marginLeft: -3,
    }
  },
  blockAnalytickHeader: {
    width: '100%',
    background: 'rgba(255, 255, 255, 0.3)',
    padding: 17,
    fontSize: 23,
    boxSizing: 'border-box',
  },
  blockAnalytickContent: {
    width: '100%',
    height: 180,
    padding: '0 70px',
    boxSizing: 'border-box',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    '& span': {
      fontSize: 60,
    },
  },
  blockAnalytickFooter: {
    width: '100%',
    padding: '12px 17px',
    boxSizing: 'border-box',
    background: 'rgba(0, 0, 0, 0.2)',
    display: 'flex',
    justifyContent: 'space-between',
  },
  scheduleWrapper: {
    width: '100%',
    marginTop: 30,
    padding: 20,
    background: '#fff',
    boxSizing: 'border-box',
    fontFamily: 'Roboto Slab',
    color: '#5111F3',
    boxShadow: '2px 2px 8px -1px #5111F3',
    transition: '150ms',
    '&:hover': {
      transition: '150ms',
      marginTop: 27,
      marginLeft: -3,
    },
    '& h2': {
      fontSize: 26,
      marginBottom: 20,
    },
  }
}));