import * as React from "react";
import "./loginForm.scss"
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import {useDispatch} from "react-redux";
import {userLogin} from "../../../store/user/actions";
import {useState} from "react";
import {loginData} from "../../../store/user/types";

interface Props {

}

export const LoginForm: React.FC<Props> = ({}) => {

  const dispatch = useDispatch();

  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const validator = (data: loginData): boolean => {
    let alertMessage: string = '';
    if (!data.email || !data.password) {
      alertMessage = 'Please enter the all fields';
    }
    if (alertMessage) {
      alert(alertMessage);
      return false;
    } else {
      return true;
    }
  }

  const requestLogin = () => {
    const requestData: loginData = {email, password}
    if (validator(requestData)) {
      dispatch(userLogin(requestData));
    }
  }

  return (
    <div className="app">
      <div className="form_wrapper">
        <div className="form_wrapper_header">
          Admin Panel
        </div>
        <form className="form">
          <TextField
            margin="dense"
            required
            label="Email Address"
            name="email"
            autoComplete="email"
            onChange={(e) => setEmail(e.currentTarget.value)}
          />
          <TextField
            margin="dense"
            required
            name="password"
            label="Password"
            type="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.currentTarget.value)}
          />
          <Button
            className="submit"
            variant="contained"
            color="primary"
            onClick={requestLogin}
          >
            Log In
          </Button>
        </form>
        <div className="form_wrapper_footer">
          NetXil
        </div>
      </div>
    </div>
  )
}