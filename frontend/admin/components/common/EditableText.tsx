import * as React from "react";
import Popover from '@material-ui/core/Popover';
import {TextField} from "@material-ui/core";
import {useState} from "react";

interface Props {
  handleClose: (text: string) => void;
  isShow: boolean,
  text: string,
  anchorEl: any,
}

export const EditableText: React.FC<Props> = ({
                                                isShow,
                                                handleClose,
                                                text,
                                                anchorEl,
                                              }) => {
  const [editedText, setEditedText] = useState<string>(text)
  return (
    <Popover
      open={isShow}
      onClose={() => handleClose(editedText)}
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      <TextField
        style={{margin: 15}}
        value={editedText}
        onChange={(e) => setEditedText(e.currentTarget.value)}
      />
    </Popover>
  )
}