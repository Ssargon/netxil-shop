import * as React from "react";
import {AppBar, Avatar, Badge, Grid, Menu, MenuItem, TextField} from "@material-ui/core";
import {config} from "../../../config";
import {useStyles} from "./style";
import {Bell, ClipboardList, DotsVertical, Mail} from "heroicons-react";
import PopupState, {bindMenu, bindTrigger} from "material-ui-popup-state";


interface Props {

}

export const Header: React.FC<Props> = ({}) => {

  const classes = useStyles();

  return (
    <AppBar position="static">
      <Grid container alignItems="stretch" className={classes.header}>
        <Grid item md={2} className={classes.logo}>
          <div className={classes.logoWrapper}>
            <a href={config.baseUrl} className={classes.link}>NetXil</a>
          </div>
        </Grid>
        <Grid item md={2}>
          <div className={classes.searchWrapper}>
            <TextField label="Search" type="search" variant="outlined" size="small"/>
          </div>
        </Grid>
        <Grid item md={5}/>
        <Grid item md={3} container alignItems="stretch">
          <Grid item md={2}>
            <div className={classes.mediaWrapper}>
              <Badge badgeContent={4} color="primary">
                <Mail color="#E89B19"/>
              </Badge>
            </div>
          </Grid>
          <Grid item md={2}>
            <div className={classes.mediaWrapper}>
              <Badge badgeContent={4} color="primary">
                <Bell color="#E89B19"/>
              </Badge>
            </div>
          </Grid>
          <Grid item md={2}>
            <div className={classes.mediaWrapper}>
              <Badge badgeContent={4} color="primary">
                <ClipboardList color="#E89B19"/>
              </Badge>
            </div>
          </Grid>
          <Grid item md={6}>
            <div className={classes.userInfoWrapper}>
              <div>
                <Avatar alt="Remy Sharp"
                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQelDnncSRloUYdFRM0IIhZ-vhdQcG_VKKG6Q&usqp=CAU"/>
                <span>Admin</span>
              </div>
              <PopupState variant="popover" popupId="demo-popup-menu">
                {(popupState) => (
                  <React.Fragment>
                    <span{...bindTrigger(popupState)}><DotsVertical/></span>
                    <Menu {...bindMenu(popupState)}>
                      <MenuItem onClick={popupState.close}>Cake</MenuItem>
                      <MenuItem onClick={popupState.close}>Sugar</MenuItem>
                    </Menu>
                  </React.Fragment>
                )}
              </PopupState>
            </div>
          </Grid>
        </Grid>
      </Grid>
    </AppBar>
  )
}