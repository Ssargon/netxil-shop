import {makeStyles} from "@material-ui/core";

const wrapper = {
  width: '100%',
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}

export const useStyles = makeStyles((theme) => ({
  header: {
    minHeight: 60,
    background: '#F9F9FA',
  },
  logo: {
    fontFamily: 'Audiowide',
    fontSize: 32,
  },
  logoWrapper: {
    ...wrapper,
    background: '#5111F3',
    borderRight: '2px solid #0900FF'
  },
  searchWrapper: {
    ...wrapper,
  },
  mediaWrapper: {
    ...wrapper,
    transition: '200ms',
    borderLeft: '1px solid #7B86AA',
    cursor: 'pointer',
    '&:hover': {
      transition: '200ms',
      background: "#E1E1E4",
    },
  },
  userInfoWrapper: {
    ...wrapper,
    borderLeft: '1px solid #7B86AA',
    justifyContent: 'space-between',
    padding: '0 20px',
    boxSizing: 'border-box',
    '& >div': {
      display: 'flex',
      alignItems: 'center',
      '& span': {
        color: '#1D4CAA',
        fontFamily: 'Audiowide',
        marginLeft: 10,
      },
    },
    '& >span:last-child': {
      color: '#1D4CAA',
      cursor: 'pointer',
    }
  },
  link: {
    textDecoration: 'none',
    color: '#F9F9FA',
  }
}));