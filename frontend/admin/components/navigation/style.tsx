import {makeStyles} from "@material-ui/core";

const defaultLink = {
  borderBottom: '1px solid #5B67A2',
  padding: 15,
  display: 'flex',
  textDecoration: 'none',
  color: '#fff',
}

export const useStyles = makeStyles(() => ({
  nav: {
    width: '100%',
    height: '100%',
    borderRight: '2px solid #0900FF',
    backgroundColor: '#3f51b5',
    position: 'relative',
  },
  link: {
    ...defaultLink,
    '&:hover': {
      backgroundColor: '#3f42a9',
    },
  },
  textLink: {
    margin: '0 0 0 15px',
    '& span': {
      fontFamily: 'Roboto Slab',
      fontSize: 20,
    },
  },
  linkActive: {
    backgroundColor: '#3f42a9',
  }
}));