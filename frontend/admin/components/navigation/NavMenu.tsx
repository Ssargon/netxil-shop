import * as React from "react";
import {useStyles} from "./style";
import {ListMenu} from "./ListMenu";

interface Props {

}

export const NavMenu: React.FC<Props> = ({}) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <nav className={classes.nav}>
        <ListMenu/>
      </nav>
    </React.Fragment>
  )
}