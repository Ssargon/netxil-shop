import * as React from "react";
import {List, ListItemText} from "@material-ui/core";
import {Collection, Cube, Home, Newspaper, ShoppingBag, Users, ViewGrid} from "heroicons-react";
import {useStyles} from "./style";
import {NavLink} from "react-router-dom";
import {MenuItem} from "../../store/types";

interface Props {

}

const listMenu = [
  'home',
  'category',
  'products',
  'users',
  'orders',
  'posts',
  'pages',
];

const listMenuIcon = [
  <Home size={30} color='#fff'/>,
  <Collection size={30} color='#fff'/>,
  <Cube size={30} color='#fff'/>,
  <Users size={30} color='#fff'/>,
  <ShoppingBag size={30} color='#fff'/>,
  <Newspaper size={30} color='#fff'/>,
  <ViewGrid size={30} color='#fff'/>
]

export const ListMenu: React.FC<Props> = ({}) => {

  const toCapitalize = (text: string): string => {
    return text.charAt(0).toUpperCase() + text.slice(1);
  };

  const menu: MenuItem[] = listMenu.map(i => {
    if (i === 'home') {
      return {
        url: '/admin',
        label: toCapitalize(i),
      }
    } else {
      return {
        url: `/admin/${i}`,
        label: toCapitalize(i),
      }
    }
  });

  const classes = useStyles();
  return (
    <React.Fragment>
      <List>
        {menu.map((i, index) => {
          return (
            <NavLink exact key={i.url} to={i.url} className={classes.link} activeClassName={classes.linkActive }>
              {listMenuIcon[index]}<ListItemText primary={i.label} className={classes.textLink}/>
            </NavLink>
          )
        })}
      </List>
    </React.Fragment>
  )
}