import {makeStyles, Theme} from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(2),
    boxSizing: 'border-box',
    paddingBottom: 20,
  },
  container: {
    maxHeight: 700,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  btnMargin: {
    marginRight: 15
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  pagination: {
    marginTop: 20,
    display: 'flex',
    justifyContent: 'center',
  },
  fab: {
    marginLeft: 10,
  },
  title: {
    display: 'flex',
    alignItems: 'center',
  }
}));