import * as React from "react";
import TableRow from '@material-ui/core/TableRow';
import TableCell from "@material-ui/core/TableCell";
import {Product} from "../../store/products/types";
import Button from "@material-ui/core/Button";
import {useStyles} from "./style";
import {useDispatch} from "react-redux";
import {deleteProduct} from "../../store/products/actions";

interface Props {
  product: Product
}

export const ProductListItem: React.FC<Props> = ({
                                                   product
                                                 }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <TableRow hover role="checkbox" tabIndex={-1}>
      <TableCell>{product.id}</TableCell>
      <TableCell>{product.name}</TableCell>
      <TableCell>{product.category.name}</TableCell>
      <TableCell>{product.cost}$</TableCell>
      <TableCell>{product.quantity}</TableCell>
      <TableCell>{product.created}</TableCell>
      <TableCell>
        <Button variant="outlined" color="primary" className={classes.btnMargin}>View</Button>
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => dispatch(deleteProduct(product.id))}
        >Delete</Button>
      </TableCell>
    </TableRow>
  )
}