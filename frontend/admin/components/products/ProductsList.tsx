import * as React from "react";
import {useStyles} from "./style";
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {ButtonGroup, Fab, Table} from "@material-ui/core";
import TableRow from '@material-ui/core/TableRow';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableBody from "@material-ui/core/TableBody";
import {useEffect, useState} from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../store";
import {Product} from "../../store/products/types";
import {loadProducts} from "../../store/products/actions";
import {ProductListItem} from "./ProductListItem";
import Button from "@material-ui/core/Button";
import Pagination from "@material-ui/lab/Pagination";
import {PaginationResponse} from "../../store/types";
import {Plus} from "heroicons-react";

interface Props {

}

const columns = [
  {id: 'id', label: 'Id'},
  {id: 'name', label: 'Name'},
  {id: 'category', label: 'Category'},
  {id: 'cost', label: 'Cost'},
  {id: 'quantity', label: 'Quantity'},
  {id: 'created', label: 'Date of Creation'},
  {id: 'actions', label: 'Actions'},
];

export const ProductsList: React.FC<Props> = ({}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const products: Product[] = useSelector(({products}: AppState) => products.products);
  const [isLoaded, setLoaded] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [perPage, setPerPage] = React.useState(10);
  const meta: PaginationResponse = useSelector(({products}: AppState) => products.meta);

  useEffect(() => {
    if (!isLoaded) {
      dispatch(loadProducts(page, perPage));
      setLoaded(true);
    }
  });

  const handleChangePerPage = (perPage: number) => {
    setPerPage(perPage);
    setPage(1);
    dispatch(loadProducts(page, perPage));
  }

  const handleChangePage = (event: unknown, value: number) => {
    dispatch(loadProducts(value, perPage));
  };

  const renderTable = () => {
    if (isLoaded) {
      return (
        <TableContainer className={classes.container} component={Paper}>
          <Toolbar className={classes.toolbar}>
            <div className={classes.title}>
              <Typography variant="h6" component="div">Products List</Typography>
              <Fab
                color="primary"
                aria-label="add"
                size='small'
                className={classes.fab}
              >
                <Plus/>
              </Fab>
            </div>
            <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
              <Button onClick={() => handleChangePerPage(5)}>5</Button>
              <Button onClick={() => handleChangePerPage(10)}>10</Button>
              <Button onClick={() => handleChangePerPage(25)}>25</Button>
              <Button onClick={() => handleChangePerPage(50)}>50</Button>
            </ButtonGroup>
          </Toolbar>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    style={{fontWeight: 'bold'}}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {products.map(product => <ProductListItem key={product.id} product={product}/>)}
            </TableBody>
          </Table>
        </TableContainer>
      )
    } else {
      return (
        <Backdrop className={classes.backdrop} open={true}>
          <CircularProgress color="inherit"/>
        </Backdrop>
      );
    }
  }

  return (
    <Paper className={classes.root}>
      {renderTable()}
      <Pagination
        count={Math.ceil(meta.total / perPage)}
        color="primary"
        onChange={handleChangePage}
        className={classes.pagination}
      />
    </Paper>
  )
}