import * as React from "react";
import TableRow from "@material-ui/core/TableRow";
import {Category} from "../../store/category/types";
import TableCell from '@material-ui/core/TableCell';
import Checkbox from '@material-ui/core/Checkbox';
import {EditableText} from "../common/EditableText";
import {useDispatch} from "react-redux";
import {loadCategories, updateCategory} from "../../store/category/actions";

interface Props {
  category: Category,
  handleClick: (event: React.MouseEvent<unknown>, id: string) => void,
  isItemSelected: boolean,
  labelId: string,
}

export const CategoryListItem: React.FC<Props> = ({
                                                    category,
                                                    handleClick,
                                                    isItemSelected,
                                                    labelId,
                                                  }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const openEditableText = (event:React.MouseEvent<unknown>) => {
    // @ts-ignore
    setAnchorEl(event.target);
  };
  const dispatch = useDispatch();
  const handleClose = (text:string) => {
    dispatch(updateCategory(category.id, text));
    setAnchorEl(null);
  };
  const isShowEditableText = Boolean(anchorEl);
  return (
    <React.Fragment>
      <TableRow
        hover
        role="checkbox"
        aria-checked={isItemSelected}
        tabIndex={-1}
        key={category.id}
        selected={isItemSelected}
      >
        <TableCell padding="checkbox">
          <Checkbox
            onClick={(event) => handleClick(event, category.id.toString())}
            checked={isItemSelected}
            inputProps={{'aria-labelledby': labelId}}
          />
        </TableCell>
        <TableCell
          component="th"
          id={labelId}
          scope="row"
          padding="none"
          onClick={openEditableText}>
          {category.name}
        </TableCell>
        <TableCell align="right">{category.created}</TableCell>
      </TableRow>
      <EditableText
        anchorEl={anchorEl}
        handleClose={handleClose}
        isShow={isShowEditableText}
        text={category.name}/>
    </React.Fragment>
  )
}