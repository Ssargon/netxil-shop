import * as React from "react";
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import {useState} from "react";
import {useDispatch} from "react-redux";
import {createCategory} from "../../store/category/actions";

interface Props {
  isOpen: boolean,
  handleClose: () => void,
}

export const CreateCategoryModal: React.FC<Props> = ({
                                                       isOpen,
                                                       handleClose
                                                     }) => {
  const [text, setText] = useState<string>('');
  const dispatch = useDispatch();

  const createNewCategory = () => {
    dispatch(createCategory(text));
    handleClose();
  }

  return (
    <React.Fragment>
      <Dialog open={isOpen} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create new category</DialogTitle>
        <DialogContent>
          <TextField
            onChange={(e) => setText(e.currentTarget.value)}
            autoFocus
            margin="dense"
            id="name"
            label="Category name"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="inherit">
            Close
          </Button>
          <Button onClick={createNewCategory} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}