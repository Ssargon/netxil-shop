import * as React from "react";
import {useToolbarStyles} from "./style";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import clsx from "clsx";
import {ButtonGroup, Fab} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Plus} from "heroicons-react";
import {CreateCategoryModal} from "./CreateCategoryModal";
import {useState} from "react";

interface Props {
  numSelected: number,
  handlePerPage: (perPage: number) => void,
  deleteCategories: () => void,
}

export const EnhancedTableToolbar: React.FC<Props> = ({
                                                        numSelected,
                                                        handlePerPage,
                                                        deleteCategories,
                                                      }) => {
  const classes = useToolbarStyles();
  const [isOpenCategoryModal, setOpenCategoryModal] = useState<boolean>(false);
  return (
    <React.Fragment>
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography className={classes.title} variant="h6" component="div">
            Categories List
            <Fab
              color="primary"
              aria-label="add"
              size='small'
              className={classes.fab}
              onClick={() => setOpenCategoryModal(true)}
            >
              <Plus/>
            </Fab>
          </Typography>

        )}
        {numSelected > 0 ? (
          <Tooltip title="Delete" onClick={deleteCategories}>
            <IconButton aria-label="delete">
              <DeleteIcon/>
            </IconButton>
          </Tooltip>
        ) : (
          <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
            <Button onClick={() => handlePerPage(5)}>5</Button>
            <Button onClick={() => handlePerPage(10)}>10</Button>
          </ButtonGroup>
        )}
      </Toolbar>
      <CreateCategoryModal
        isOpen={isOpenCategoryModal}
        handleClose={() => setOpenCategoryModal(false)}
      />
    </React.Fragment>
  );
}