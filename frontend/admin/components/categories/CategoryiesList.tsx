import * as React from "react";
import {useStyles} from "./style";
import {useDispatch, useSelector} from "react-redux";
import {Category, Data} from "../../store/category/types";
import {AppState} from "../../store";
import {deleteCategories, loadCategories} from "../../store/category/actions";
import {Table, TableContainer,} from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import {useEffect, useState} from "react";
import {EnhancedTableToolbar} from "./EnhancedTableToolbar";
import {EnhancedTableHead} from "./EnhancedTableHead";
import TableBody from '@material-ui/core/TableBody';
import {CategoryListItem} from "./CategoryListItem";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import {PaginationResponse} from "../../store/types";
import Pagination from '@material-ui/lab/Pagination';

interface Props {

}

type Order = 'asc' | 'desc';

export const CategoriesList: React.FC<Props> = ({}) => {
  const classes = useStyles();
  const [page, setPage] = useState<number>(1);
  const [isLoaded, setLoaded] = useState<boolean>(false);
  const [selected, setSelected] = React.useState<string[]>([]);
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('created');
  const [perPage, setPerPage] = React.useState(5);
  const dispatch = useDispatch();
  const categories: Category[] = useSelector(({category}: AppState) => category.categories);
  const meta: PaginationResponse = useSelector(({category}: AppState) => category.meta);

  useEffect(() => {
    if (!isLoaded) {
      dispatch(loadCategories(page, perPage));
      setLoaded(true);
    }
  });

  function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
  ): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = categories.map((c) => c.id.toString());
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const isSelected = (id: string) => selected.indexOf(String(id)) !== -1;

  const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, value: number) => {
    dispatch(loadCategories(value, perPage));
  };

  const handleChangeRowsPerPage = (perPage: number) => {
    setPerPage(perPage);
    setPage(1);
    dispatch(loadCategories(page, perPage));
  };

  const renderTable = () => {
    if (isLoaded) {
      return (
        <TableContainer component={Paper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size='medium'
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={categories.length}/>
            <TableBody>
              {
                stableSort(categories, getComparator(order, orderBy))
                  .map((category, index) => {
                    const isItemSelected = isSelected(category.id.toString());
                    const labelId = `enhanced-table-checkbox-${index}`;
                    return (
                      <CategoryListItem
                        key={category.id}
                        handleClick={handleClick}
                        category={category}
                        isItemSelected={isItemSelected}
                        labelId={labelId}
                      />
                    )
                  })}
            </TableBody>
          </Table>
        </TableContainer>
      )
    } else {
      return (
        <Backdrop className={classes.backdrop} open={true}>
          <CircularProgress color="inherit"/>
        </Backdrop>
      )
    }
  }

  return (
    <Paper className={classes.paper}>
      <EnhancedTableToolbar
        numSelected={selected.length}
        handlePerPage={(perPage) => handleChangeRowsPerPage(perPage)}
        deleteCategories={() => dispatch(deleteCategories(selected))}
      />
      {renderTable()}
      <Pagination
        count={Math.ceil(meta.total / perPage)}
        color="primary"
        onChange={handleChangePage}
        className={classes.pagination}
      />
    </Paper>
  );
}