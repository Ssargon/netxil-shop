import * as React from "react";
import {MainPage} from "./main/MainPage";
import {LoginForm} from "./common/loginForm/LoginForm";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../store";
import {useEffect} from "react";
import {checkUser} from "../store/user/actions";

interface Props {

}

export const MainAdmin: React.FC<Props> = ({}) => {
  let currentUser = useSelector(({user}: AppState) => user.currentUser);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkUser());
  },[]);

  const renderMainPage = () => {
    if (currentUser && currentUser.data.roles === 'ROLE_ADMIN') {
      return <MainPage/>;
    } else {
      return <LoginForm/>
    }
  }

  return (
    <React.Fragment>
      {renderMainPage()}
    </React.Fragment>
  )
}