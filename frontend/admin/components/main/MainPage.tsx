import * as React from "react";
import {Header} from "../header/Header";
import {Grid} from "@material-ui/core";
import {NavMenu} from "../navigation/NavMenu";
import {Route, Switch} from "react-router-dom";
import {Home} from "../home/Home";
import {Category} from "../categories/Category";
import {Products} from "../products/Products";
import {Users} from "../users/Users";
import {Orders} from "../orders/Orders";
import {Posts} from "../posts/Posts";
import {Pages} from "../pages/Pages";

interface Props {

}

export const MainPage: React.FC<Props> = ({}) => {
  return (
    <React.Fragment>
      <Header/>
      <Grid container alignItems="stretch" style={{flex: '1 0 0'}}>
        <Grid item md={2}>
          <NavMenu/>
        </Grid>
        <Grid item md={10} style={{padding: 25, backgroundColor:'#ddd'}}>
          <Switch>
            <Route exact path="/admin">
              <Home/>
            </Route>
            <Route path="/admin/category">
              <Category/>
            </Route>
            <Route path="/admin/products">
              <Products/>
            </Route>
            <Route path="/admin/users">
              <Users/>
            </Route>
            <Route path="/admin/orders">
              <Orders/>
            </Route>
            <Route path="/admin/posts">
              <Posts/>
            </Route>
            <Route path="/admin/pages">
              <Pages/>
            </Route>
          </Switch>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}