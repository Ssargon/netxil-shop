import {ActionType} from "typesafe-actions";
import {
  categoriesLoaded,
  categoryCreated, categoriesDeleted,
  categoryUpdated,
  createCategory, deleteCategories,
  loadCategories,
  updateCategory
} from "./actions";
import {Category, CategoryActionTypes, CategoryResponse} from "./types";
import {config} from "../../../config"
import {all, put, takeEvery} from "redux-saga/effects";
import {getToken} from "../../tokenManager";

function* handleLoadCategories(action: ActionType<typeof loadCategories>) {
  try {
    const categories: CategoryResponse = yield fetch(
      `${config.baseUrl}api/category?page=${action.payload.page}&perPage=${action.payload.perPage}`,
      {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        }
      }
    ).then(res => res.ok ? res.json() : null)
    if (categories) {
      yield put(categoriesLoaded(categories));
    }
  } catch (err) {
    throw err;
  }
}

function* handleUpdateCategory(action: ActionType<typeof updateCategory>) {
  try {
    const newCategory: Category = yield fetch(
      `${config.baseUrl}api/category?id=${action.payload.id}`,
      {
        method: 'post',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(action.payload.categoryName)
      }
    ).then(res => res.ok ? res.json() : null);
    if (newCategory) {
      yield put(categoryUpdated(newCategory));
    }
  } catch (err) {
    throw err;
  }
}

function* handleCreateCategory(action: ActionType<typeof createCategory>) {
  try {
    const newCategory: Category = yield fetch(
      `${config.baseUrl}api/category`,
      {
        method: 'post',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(action.payload)
      }
    ).then(res => res.ok ? res.json() : null);
    if (newCategory) {
      yield put(categoryCreated(newCategory));
    }
  } catch (err) {
    throw err;
  }
}

function* handleDeleteCategories(action: ActionType<typeof deleteCategories>) {
  try {
    const isDeleted = yield fetch(
      `${config.baseUrl}api/category?ids=${action.payload}`,
      {
        method: 'delete',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        }
      }
    ).then(res => res.ok)
    if (isDeleted) {
      yield put(categoriesDeleted(action.payload));
    }
  } catch (err) {
    throw err;
  }
}


function* watchLoadCategories() {
  yield takeEvery(CategoryActionTypes.loadCategories, handleLoadCategories);
}

function* watchUpdateCategory() {
  yield takeEvery(CategoryActionTypes.updateCategory, handleUpdateCategory);
}

function* watchCreateCategory() {
  yield takeEvery(CategoryActionTypes.createCategory, handleCreateCategory);
}

function* watchDeleteCategories() {
  yield takeEvery(CategoryActionTypes.deleteCategories, handleDeleteCategories);
}


export function* categorySaga() {
  yield all([
    watchLoadCategories(),
    watchUpdateCategory(),
    watchCreateCategory(),
    watchDeleteCategories(),
  ]);
}