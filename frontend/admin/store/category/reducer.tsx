import * as Actions from "./actions";
import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";
import {Category, CategoryActionTypes, CategoryState} from "./types";

type Action = ActionType<typeof Actions>;

export const initialCategoryState: CategoryState = {
  categories: [],
  meta: {
    total: 0,
    page: 1,
    perPage: 5,
  },
};

const categoryReducer: Reducer<CategoryState, Action> = (state: CategoryState = initialCategoryState, action: Action): CategoryState => {
  switch (action.type) {
    case CategoryActionTypes.categoriesLoaded:
      return {
        ...state,
        categories: action.payload.categories,
        meta: action.payload.meta
      };
    case CategoryActionTypes.categoryUpdated:
      return {
        ...state,
        categories: state.categories.map((category: Category) => {
          if (category.id === action.payload.id) {
            return action.payload;
          }
          return category;
        })
      }
    case CategoryActionTypes.categoryCreated:
      return {
        ...state,
        categories: [...state.categories, action.payload],
      }
    case CategoryActionTypes.categoriesDeleted:
      return {
        ...state,
        categories: state.categories.filter(category => action.payload.indexOf(category.id.toString()) == -1)
      }
  }
  return state;
};

export default categoryReducer;