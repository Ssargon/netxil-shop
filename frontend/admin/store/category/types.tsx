import {PaginationResponse} from "../types";

export enum CategoryActionTypes {
  loadCategories = '@@categories/LOAD',
  categoriesLoaded = '@@categories/LOADED',
  updateCategory  = '@@categories/UPDATE',
  categoryUpdated = '@@categories/UPDATED',
  createCategory  = '@@categories/CREATE',
  categoryCreated = '@@categories/CREATED',
  deleteCategories  = '@@categories/DELETE',
  categoriesDeleted = '@@categories/DELETED',
}

export interface CategoryState {
  categories: Category[],
  meta:PaginationResponse,
}

export interface CategoryResponse {
  meta: PaginationResponse,
  categories: Category[],
}

export interface Category {
  id: number,
  name: string,
  created: string,
}

export interface Data {
  name: string;
  created: string;
}