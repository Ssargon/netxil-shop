import {action} from "typesafe-actions";
import {Category, CategoryActionTypes, CategoryResponse} from "./types";

export const loadCategories = (page: number, perPage: number) =>
  action(CategoryActionTypes.loadCategories, {page, perPage});

export const categoriesLoaded = (categoryResponse: CategoryResponse) =>
  action(CategoryActionTypes.categoriesLoaded, categoryResponse);

export const updateCategory = (id: number, categoryName: string) =>
  action(CategoryActionTypes.updateCategory, {id, categoryName});

export const categoryUpdated = (newCategory: Category) =>
  action(CategoryActionTypes.categoryUpdated, newCategory);

export const createCategory = (categoryName: string) =>
  action(CategoryActionTypes.createCategory, categoryName);

export const categoryCreated = (newCategory: Category) =>
  action(CategoryActionTypes.categoryCreated, newCategory);

export const deleteCategories = (ids: string[]) =>
  action(CategoryActionTypes.deleteCategories, ids);

export const categoriesDeleted = (ids: string[]) =>
  action(CategoryActionTypes.categoriesDeleted, ids);