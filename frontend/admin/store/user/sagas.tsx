import {ActionType} from "typesafe-actions";
import {userLogged, userLogin} from "./actions";
import {User, UserActionTypes} from "./types";
import {config} from "../../../config"
import {all, put, takeEvery} from "redux-saga/effects";

function* handleLogin(action: ActionType<typeof userLogin>) {
  try {
    const currentUser: User = yield fetch(
      `${config.baseUrl}login`,
      {
        method: 'post',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(action.payload)
      }
    ).then(res => res.ok ? res.json() : null)
    if (currentUser) {
      yield put(userLogged(currentUser));
    }
  } catch (err) {
    throw err;
  }
}

function* watchUserLogin() {
  yield takeEvery(UserActionTypes.userLogin, handleLogin);
}

export function* userSaga() {
  yield all([
    watchUserLogin(),
  ]);
}