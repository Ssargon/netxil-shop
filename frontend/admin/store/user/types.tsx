export enum UserActionTypes {
  userLogin = '@@user/LOGIN',
  userLogged = '@@user/LOGGED',
  checkUser = '@@user/CHECK',
}

export interface UserState {
  readonly currentUser: User | null,
}

export interface User {
  token: string,
  data: {
    id: number,
    email: string,
    roles: string,
  }
}

export interface loginData {
  email: string,
  password: string,
}