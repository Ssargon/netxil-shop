import * as Actions from "./actions";
import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";
import {UserActionTypes, UserState} from "./types";

type Action = ActionType<typeof Actions>;

export const initialUserState: UserState = {
  currentUser: null,
};

const userReducer: Reducer<UserState, Action> = (state: UserState = initialUserState, action: Action): UserState => {
  switch (action.type) {
    case UserActionTypes.userLogged:
      localStorage.setItem('userAdmin', JSON.stringify(action.payload));
      return {...state, currentUser: action.payload};
    case UserActionTypes.checkUser:
      return {...state, currentUser: JSON.parse(localStorage.getItem('userAdmin') as string)};
  }
  return state;
};

export default userReducer;