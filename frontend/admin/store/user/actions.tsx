import {action} from "typesafe-actions";
import {loginData, User, UserActionTypes} from "./types";

export const userLogin = (data: loginData) => action(UserActionTypes.userLogin, data);

export const userLogged = (user: User) => action(UserActionTypes.userLogged, user);

export const checkUser = () => action(UserActionTypes.checkUser);