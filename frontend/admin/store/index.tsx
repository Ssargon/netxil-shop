import {createStore, applyMiddleware, combineReducers} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import createSagaMiddleware from 'redux-saga';
import {all} from 'redux-saga/effects';
import {UserState} from "./user/types";
import userReducer from "./user/reducer";
import {userSaga} from "./user/sagas";
import {CategoryState} from "./category/types";
import categoryReducer from "./category/reducer";
import {categorySaga} from "./category/sagas";
import {ProductState} from "./products/types";
import productReducer from "./products/reducer";
import {productsSaga} from "./products/sagas";

export interface AppState {
  user: UserState,
  category: CategoryState,
  products: ProductState,
}

const rootReducer = combineReducers<AppState>({
  user: userReducer,
  category: categoryReducer,
  products: productReducer,
});

function* rootSaga() {
  yield all([
    userSaga(),
    categorySaga(),
    productsSaga(),
  ]);
}

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = composeWithDevTools({});
const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));
const adminStore = createStore(rootReducer, enhancer);

sagaMiddleware.run(rootSaga);

export default adminStore;