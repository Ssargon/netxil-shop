export interface MenuItem {
  url: string,
  label: string,
}

export interface PaginationResponse {
  total: number,
  page: number,
  perPage: number,
}