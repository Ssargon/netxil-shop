import {PaginationResponse} from "../types";
import {Category} from "../category/types";

export enum ProductActionTypes {
  loadProducts = '@@products/LOAD',
  productsLoaded = '@@products/LOADED',
  deleteProduct = '@@products/DELETE',
  productDeleted = '@@products/DELETED',
}

export interface ProductState {
  products: Product[],
  meta: PaginationResponse,
}

export interface ProductResponse {
  meta: PaginationResponse,
  products: Product[],
}

export interface Product {
  id: number,
  name: string,
  created: string,
  cost: number,
  quantity: number,
  category: Category,
}