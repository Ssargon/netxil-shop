import * as Actions from "./actions";
import {ActionType} from "typesafe-actions";
import {Reducer} from "redux";
import {ProductActionTypes, ProductState} from "./types";

type Action = ActionType<typeof Actions>;

export const initialProductState: ProductState = {
  products: [],
  meta: {
    total: 0,
    page: 1,
    perPage: 20,
  },
};

const productReducer: Reducer<ProductState, Action> = (state: ProductState = initialProductState, action: Action): ProductState => {
  switch (action.type) {
    case ProductActionTypes.productsLoaded:
      return {
        ...state,
        products: action.payload.products,
        meta: action.payload.meta,
      }
    case ProductActionTypes.productDeleted:
      return {
        ...state,
        products: state.products.filter(product => product.id !== action.payload),
      }
  }
  return state;
}

export default productReducer;