import {action} from "typesafe-actions";
import {ProductActionTypes, ProductResponse} from "./types";

export const loadProducts = (page: number, perPage: number) =>
  action(ProductActionTypes.loadProducts, {page, perPage});

export const productsLoaded = (productResponse: ProductResponse) =>
  action(ProductActionTypes.productsLoaded, productResponse);

export const deleteProduct = (id: number) =>
  action(ProductActionTypes.deleteProduct, id);

export const productDeleted = (id: number) =>
  action(ProductActionTypes.productDeleted, id);