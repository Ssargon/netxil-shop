import {ActionType} from "typesafe-actions";
import {config} from "../../../config"
import {all, put, takeEvery} from "redux-saga/effects";
import {getToken} from "../../tokenManager";
import {deleteProduct, loadProducts, productDeleted, productsLoaded,} from "./actions";
import {ProductActionTypes, ProductResponse} from "./types";

function* handleLoadProducts(action: ActionType<typeof loadProducts>) {
  try {
    const products: ProductResponse = yield fetch(
      `${config.baseUrl}api/product?page=${action.payload.page}&perPage=${action.payload.perPage}`,
      {
        method: 'get',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        }
      }
    ).then(res => res.ok ? res.json() : null)
    if (products) {
      yield put(productsLoaded(products));
    }
  } catch (err) {
    throw err;
  }
}

function* handleDeleteProduct(action: ActionType<typeof deleteProduct>) {
  try {
    const isDeleted = yield fetch(
      `${config.baseUrl}api/product?id=${action.payload}`,
      {
        method: 'delete',
        headers: {
          'Authorization': `Bearer ${getToken()}`,
          'Content-Type': 'application/json',
        }
      }
    ).then(res => res.ok)
    if (isDeleted) {
      yield put(productDeleted(action.payload));
    }
  } catch (err) {
    throw err;
  }
}

function* watchLoadProducts() {
  yield takeEvery(ProductActionTypes.loadProducts, handleLoadProducts);
}

function* watchDeleteProduct() {
  yield takeEvery(ProductActionTypes.deleteProduct, handleDeleteProduct);
}

export function* productsSaga() {
  yield all([
    watchLoadProducts(),
    watchDeleteProduct(),
  ]);
}