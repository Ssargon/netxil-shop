import * as React from "react";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Main from './shop/Main';
import {Provider} from "react-redux";
import adminStore from "./admin/store/index";
import {MainAdmin} from "./admin/components/MainAdmin";

interface Props {

}

export const MainRouter: React.FC<Props> = ({}) => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Main/>
        </Route>
        <Route path="/admin">
          <Provider store={adminStore}>
            <MainAdmin/>
          </Provider>
        </Route>
      </Switch>
    </Router>
  )
}
