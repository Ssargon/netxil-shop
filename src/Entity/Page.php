<?php


namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PageRepository::class)
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $bannerImage;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $bannerColor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordering;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="page", cascade={"persist"})
     *
     * @var Product[]|Collection
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="page", cascade={"persist"})
     *
     * @var Post[]|Collection
     */
    private $posts;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function addProduct(Product $product): Page
    {
        $this->products->add($product);
        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->pageName;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->pageName = $name;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getBannerImage(): ?string
    {
        return $this->bannerImage;
    }

    /**
     * @param string|null $bannerImage
     */
    public function setBannerImage(?string $bannerImage): void
    {
        $this->bannerImage = $bannerImage;
    }

    /**
     * @return string|null
     */
    public function getBannerColor(): ?string
    {
        return $this->bannerColor;
    }

    /**
     * @param string|null $bannerColor
     */
    public function setBannerColor(?string $bannerColor): void
    {
        $this->bannerColor = $bannerColor;
    }

    /**
     * @return string|null
     */
    public function getOrdering(): ?string
    {
        return $this->ordering;
    }

    /**
     * @param string|null $ordering
     */
    public function setOrdering(?string $ordering): void
    {
        $this->ordering = $ordering;
    }

    /**
     * @param \DateTime|null $created
     */
    public function setCreated(?\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime|null $deleted
     */
    public function setDeleted(?\DateTime $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeleted(): ?\DateTime
    {
        return $this->deleted;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPost(Post $post): Page
    {
        $this->posts->add($post);
        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }
}