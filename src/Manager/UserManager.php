<?php

namespace App\Manager;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Serializer\UserSerializer;
use App\Services\PasswordService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\VarDumper\VarDumper;

class UserManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var PasswordService
     */
    protected $passwordService;

    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var UserSerializer
     */
    protected $userSerializer;

    /**
     * UserManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param PasswordService $passwordService
     * @param UserRepository $userRepository
     * @param UserSerializer $userSerializer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PasswordService $passwordService,
        UserRepository $userRepository,
        UserSerializer $userSerializer
    )
    {
        $this->em = $entityManager;
        $this->passwordService = $passwordService;
        $this->userRepository = $userRepository;
        $this->userSerializer = $userSerializer;
    }

    const ITEMS_PER_PAGE = 15;

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email)
    {
        $user = $this->userRepository->findByEmail($email);

        if ($user) {
            return $user[0];
        }

        return null;
    }

    /**
     * @param int $id
     */
    public function remove(int $id)
    {
        $user = $this->userRepository->find($id);

        if ($user) {
            $user->setDeleted(new \DateTime());
            $this->em->persist($user);
            $this->em->flush();
        }
    }

    /**
     * @param User $user
     * @return array|string
     * @throws \Exception
     */
    public function registerAccount(User $user)
    {
        if ($this->findByEmail($user->getEmail())) {
            throw new BadRequestHttpException('This email address has already been used.');
        }
        $username = $user->getUsername() ? $user->getUsername() : $user->getEmail();
        $user->setUsername($username);
        $pass = $this->passwordService->encode($user, $user->getPassword());
        $user->setPassword($pass);
        $user->setCreated(new \DateTime());
        $user->setUpdated(new \DateTime());
        $user->setRoles(User::ROLE_USER);
        $this->em->persist($user);
        $this->em->flush();

        return [
            'user' => $user
        ];
    }

    public function getUsers(int $page = 1): JsonResponse
    {
        $perPage = self::ITEMS_PER_PAGE;
        $offset = ($page - 1) * $perPage;
        $queryBuilder = $this->em->getRepository(User::class)->createQueryBuilder('u')
            ->andWhere('u.deleted IS NULL')
            ->orderBy('u.id', 'DESC');
        $paginator = new Paginator($queryBuilder);
        $total = $paginator->count();
        if ($offset > $total) {
            $page = 1;
            $offset = 0;
        }
        $paginator->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($perPage);
        $users = $paginator->getQuery()->execute();
        $paginator->count();

        return new JsonResponse([
            'meta' => [
                'total' => $paginator->count(),
                'page' => $page,
                'perPage' => self::ITEMS_PER_PAGE,
            ],
            'users' => array_map([$this->userSerializer, 'serializeUser'], $users),
        ]);
    }
}