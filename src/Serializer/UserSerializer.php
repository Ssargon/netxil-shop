<?php

namespace App\Serializer;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserSerializer extends AbstractController
{
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function serializeUser(User $user): array
    {
        return [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'username' => $user->getUsername()
        ];
    }
}