<?php

namespace App\Serializer;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductSerializer extends AbstractController
{
    /**
     * @var CategorySerializer
     */
    protected $categorySerializer;

    /**
     * ProductSerializer constructor.
     * @param CategorySerializer $categorySerializer
     */
    public function __construct(
        CategorySerializer $categorySerializer
    )
    {
        $this->categorySerializer = $categorySerializer;
    }

    /**
     * @param Product $product
     * @return array
     * @throws \Exception
     */
    public function serializeProduct(Product $product): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'created' => $product->getCreated()->format("Y-m-d H:i:s"),
            'cost' => $product->getCost(),
            'quantity' => $product->getQuantity(),
            'category' => $this->categorySerializer->serializeCategory($product->getCategory()),
        ];
    }
}