<?php

namespace App\Serializer;

use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategorySerializer extends AbstractController
{
    public function __construct()
    {
    }

    /**
     * @param Category $category
     * @return array
     * @throws \Exception
     */
    public function serializeCategory(Category $category): array
    {
        return [
            'id' => $category->getId(),
            'name' => $category->getCategoryName(),
            'created' => $category->getCreated()->format("Y-m-d H:i:s"),
        ];
    }
}