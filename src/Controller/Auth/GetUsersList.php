<?php

namespace App\Controller\Auth;
use App\Manager\UserManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GetUsersList
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * GetUsersList constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(Request $request):JsonResponse
    {
        $page = (int) $request->query->get('page', 1);
        return $this->userManager->getUsers($page);
    }
}
