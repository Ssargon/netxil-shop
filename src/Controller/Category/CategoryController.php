<?php

namespace App\Controller\Category;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Serializer\CategorySerializer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;
    /**
     * @var CategorySerializer
     */
    protected $categorySerializer;

    /**
     * CategoryController constructor.
     * @param EntityManagerInterface $entityManager
     * @param CategoryRepository $categoryRepository
     * @param CategorySerializer $categorySerializer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        CategoryRepository $categoryRepository,
        CategorySerializer $categorySerializer
    )
    {
        $this->em = $entityManager;
        $this->categoryRepository = $categoryRepository;
        $this->categorySerializer = $categorySerializer;
    }

    /**
     * @Route("api/category", name="categoryCreate", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createOrUpdateCategory(
        Request $request
    ): JsonResponse
    {
        $categoryId = $request->get('id', null);
        $categoryName = json_decode((string)$request->getContent());
        if ($categoryId) {
            $category = $this->em->getRepository(Category::class)->find($categoryId);
        } else {
            $category = new Category();
            $category->setCreated(new \DateTime());
        }
        $category->setCategoryName($categoryName);
        $this->em->persist($category);
        $this->em->flush();
        return new JsonResponse($this->categorySerializer->serializeCategory($category));
    }


    /**
     * @Route("api/category", name="categoryDelete", methods={"DELETE"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteCategory(
        Request $request
    ): JsonResponse
    {
        $categoryIds = $request->get('ids');
        $categories = $this->em->getRepository(Category::class)->findBy(['id' => $categoryIds]);
        foreach ($categories as $category) {
            $category->setDeleted(new \DateTime());
            $this->em->persist($category);
        }
        $this->em->flush();
        return new JsonResponse();
    }

    /**
     * @Route("api/category", name="categoryList", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCategoriesList(
        Request $request
    ): JsonResponse
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 5);
        $queryBuilder = $this->em->getRepository(Category::class)->createQueryBuilder('c')
            ->andWhere('c.deleted IS NULL')
            ->orderBy('c.id', 'DESC');
        $paginator = new Paginator($queryBuilder);
        $total = $paginator->count();
        $offset = ($page - 1) * $perPage;
        if ($offset > $total) {
            $page = 1;
            $offset = 0;
        }
        $paginator->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($perPage);
        $categories = $paginator->getQuery()->execute();
        $paginator->count();
        return new JsonResponse([
            'meta' => [
                'total' => $paginator->count(),
                'page' => intval($page),
                'perPage' => $perPage,
            ],
            'categories' => array_map([$this->categorySerializer, 'serializeCategory'], $categories),
        ]);
    }
}