<?php

namespace App\Controller\Product;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Serializer\ProductSerializer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var ProductSerializer
     */
    protected $productSerializer;

    /**
     * CategoryController constructor.
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     * @param ProductSerializer $productSerializer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository,
        ProductSerializer $productSerializer
    )
    {
        $this->em = $entityManager;
        $this->productRepository = $productRepository;
        $this->productSerializer = $productSerializer;
    }

    /**
     * @Route("api/product", name="productCreate", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function createOrUpdateProduct(
        Request $request
    ): JsonResponse
    {
        $productId = $request->get('id', null);
        $data = json_decode((string)$request->getContent());
        $productName = $data->productName;
        $cost = $data->cost;
        $quantity = $data->quantity;
        $categoryId = $data->categoryId;
        if ($productId) {
            $product = $this->em->getRepository(Product::class)->find($productId);
        } else {
            $product = new Product();
            $product->setCreated(new \DateTime());
        }
        $product->setName($productName);
        $product->setCost($cost);
        $product->setQuantity($quantity);
        $category = $this->em->getRepository(Category::class)->find($categoryId);
        $category->addProduct($product);
        $product->setCategory($category);
        $this->em->persist($product);
        $this->em->flush();
        return new JsonResponse($this->productSerializer->serializeProduct($product));
    }

    /**
     * @Route("api/product", name="productDelete", methods={"DELETE"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteProduct(
        Request $request
    ): JsonResponse{
        $productId = $request->get('id');
        $product = $this->em->getRepository(Product::class)->find($productId);
        $product->setDeleted(new \DateTime());
        $this->em->persist($product);
        $this->em->flush();
        return new JsonResponse();
    }

    /**
     * @Route("api/product", name="productList", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getProductList(
        Request $request
    ): JsonResponse
    {
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 5);
        $queryBuilder = $this->em->getRepository(Product::class)->createQueryBuilder('p')
            ->andWhere('p.deleted IS NULL')
            ->orderBy('p.id', 'DESC');
        $paginator = new Paginator($queryBuilder);
        $total = $paginator->count();
        $offset = ($page - 1) * $perPage;
        if ($offset > $total) {
            $page = 1;
            $offset = 0;
        }
        $paginator->getQuery()
            ->setFirstResult($offset)
            ->setMaxResults($perPage);
        $products = $paginator->getQuery()->execute();
        $paginator->count();
        return new JsonResponse([
            'meta' => [
                'total' => $paginator->count(),
                'page' => intval($page),
                'perPage' => $perPage,
            ],
            'products' => array_map([$this->productSerializer, 'serializeProduct'], $products),
        ]);
    }
}